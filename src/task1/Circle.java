package task1;

public class Circle implements Shape {

	double radius;

	Circle() {

	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getArea() {
		return 3.14 * this.radius * this.radius;
	}

	public double getPerim() {
		return 2 * 3.14 * this.radius;
	}

}

package task1;

public class Rectangle implements Shape {

	private double a;
	private double b;

	Rectangle() {

	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getArea() {
		return this.a * this.b;
	}

	public double getPerim() {
		return this.a + this.b;
	}

}

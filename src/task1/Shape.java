package task1;

public interface Shape {

	public double getArea();
    public double getPerim();
}

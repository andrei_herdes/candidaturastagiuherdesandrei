package task1;

public class Triangle implements Shape {

	private double a;
	private double b;
	private double c;

	Triangle() {

	}
	
	Triangle(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	
	
	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	public double getPerim() {
		return a+b+c;
	}

	public double getSemiPerim() {
		return getPerim()/2;
	}

	public double getArea() {
		return Math.sqrt(getSemiPerim() * (getSemiPerim() - a) 
				* (getSemiPerim() - b) * (getSemiPerim() - c));
	}

}

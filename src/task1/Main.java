package task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

	public static void main(String[] args) {

		int OK = 1;

		do {
			System.out.println("Choose your geometric shape");

			String shape = null;
			try {
				shape = scan.readLine();
			} catch (IOException e) {
				System.out.println("Error at reading from stream");
			}
			shape = shape.trim();

			switch (shape) {
			case "Triangle":
				System.out.println("Insert 3 values");

				Triangle t = new Triangle();

				try {
					initTriangle(t);
				} catch (Exception e) {
					System.out.println("Please enter numeric values");
					break;
				}

				System.out.println("Area of triangle: " + t.getArea());
				System.out.println("Perimeter of triangle: " + t.getPerim());
				break;

			case "Rectangle":
				System.out.println("Insert 2 values");

				Rectangle r = new Rectangle();

				try {
					initRectangle(r);
				} catch (Exception e) {
					System.out.println("Please enter numeric values");
					break;
				}

				System.out.println("Area of rectangle: " + r.getArea());
				System.out.println("Perimeter of rectangle: " + r.getPerim());
				break;
			case "Circle":
				System.out.println("Insert 1 value");

				Circle c = new Circle();

				try {
					initCircle(c);
				} catch (Exception e) {
					System.out.println("Please enter numeric values");
					break;
				}

				System.out.println("Area of circle: " + c.getArea());
				System.out.println("Perimeter of circle: " + c.getPerim());
				break;

			case "Exit":
				OK = 0;
				try {
					scan.close();
					System.out.println("Stream closed");
				} catch (IOException e) {
					System.out.println("Error at closing stream");
				}
				break;
			default:
				System.out.println("Invalid shape");
				break;
			}

		} while (OK != 0);

	}

	public static Triangle initTriangle(Triangle t) throws Exception {

		t.setA(Double.parseDouble(scan.readLine()));
		t.setB(Double.parseDouble(scan.readLine()));
		t.setC(Double.parseDouble(scan.readLine()));
		return t;
	}

	public static Rectangle initRectangle(Rectangle r) throws Exception {

		r.setA(Double.parseDouble(scan.readLine()));
		r.setB(Double.parseDouble(scan.readLine()));
		return r;
	}

	public static Circle initCircle(Circle c) throws Exception {
		c.setRadius(Double.parseDouble(scan.readLine()));
		return c;
	}
}

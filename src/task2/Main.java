package task2;

import java.util.ArrayList;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

		ArrayList<Integer> numbers = new ArrayList<Integer>();
		ArrayList<Integer> primes = new ArrayList<Integer>();
		ArrayList<Integer> palindromes = new ArrayList<Integer>();
		Integer min = 1000, max = 0;

		Random randomGenerator = new Random();

		for (int i = 0; i < 6; i++) {
			numbers.add(randomGenerator.nextInt(1000));
		}

		System.out.println(numbers);

		for (int i = 0; i < numbers.size(); i++) {
			if (numbers.get(i) > max) {
				max = numbers.get(i);
			}
			if (numbers.get(i) < min) {
				min = numbers.get(i);
			}
			if (isPrime(numbers.get(i))) {
				primes.add(numbers.get(i));
			}
			if (isPalindrome(numbers.get(i))) {
				palindromes.add(numbers.get(i));
			}

		}

		System.out.println("Largest number: " + max);
		System.out.println("Smallest number: " + min);
		System.out.println("Palindromes: " + palindromes);
		System.out.println("Prime numbers: " + primes);

	}

	public static boolean isPrime(Integer num) {

		if (num < 2)
			return true;
		else if (num == 2)
			return false;
		else if (num % 2 == 0)
			return false;
		else {
			for (int i = 3; i < Math.sqrt(num); i += 2) {
				if (num % i == 0)
					return false;
			}
		}

		return true;
	}

	public static boolean isPalindrome(Integer num) {

		int copy = num.intValue();
		int reverse = 0;

		while (copy != 0) {
			int remainder = copy % 10;
			reverse = reverse * 10 + remainder;
			copy = copy / 10;
		}

		return num == reverse;
	}
}
